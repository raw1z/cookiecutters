import React from 'react'

function App() {
  return (
    <div className="App">
      <div className="px-20 w-screen h-screen dark:bg-gray-900 dark:text-gray-100 flex flex-col justify-center items-center text-3xl text-center">
          <div className="font-semibold animate__animated animate__fadeInDown">Oui, Dieu a tant aimé le monde qu'il a donné son Fils, son unique, pour que tous ceux qui placent leur confiance en lui échappent à la perdition et qu'ils aient la vie éternelle. </div>
          <div className="font-bold text-blue-500 animate__animated animate__fadeInUp">Jean 3.16</div>
      </div>
    </div>
  )
}

export default App
